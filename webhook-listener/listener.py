"""Listener module."""
from sys import platform as _platform
from os import environ
import subprocess 
import urllib2
import os
import sys
import re 

DETACHED_PROCESS = 0x00000008

from flask import Flask, request, send_from_directory
app = Flask(__name__)

# check for ngrok subdomain
ngrok = "seanbamforth"
welcome = "Webhook server online!"
valid_branches = ["staging","production","qa","master"]

print "starting...."

def display_intro():
    """Helper method to display introduction message."""
    if ngrok:
        message = "".join([
            "You can access this webhook publicly via at ",
            "http://%s.ngrok.io/webhook. \n" % ngrok,
            "You can access ngrok's web interface via http://localhost:4040"
        ])
    else:
        message = "Webhook server online! Go to http://localhost:5000"
    print message


def display_html(request):
    """
    Helper method to display message in HTML format.

    :param request: HTTP request from flask
    :type  request: werkzeug.local.LocalProxy
    :returns message in HTML format
    :rtype basestring
    """
    url_root = request.url_root
    if ngrok:
        return "".join([
            welcome,
            """<br/>Go to <a href="https://bitbucket.com">Bitbucket</a>""",
            """ to configure your repository webhook for """,
            """<a href="http://%s.ngrok.io/webhook">""" % ngrok,
            """http://%s.ngrok.io/webhook</a> <br/>""" % ngrok,
            """You can access ngrok's web interface via  """,
            """<a href="http://localhost:4040">http://localhost:4040</a>"""
        ])
    else:
        return "".join([
            """Webhook server online! """,
            """Go to <a href="https://bitbucket.com">Bitbucket</a>""",
            """ to configure your repository webhook for """,
            """<a href="%s/webhook">%s/webhook</a>""" % (url_root, url_root)
        ])


@app.route("/", methods=["GET"])
def index():
    """Endpoint for the root of the Flask app."""
    return display_html(request)

def branch_is_valid(commit_branch):
    return commit_branch in valid_branches

def build_repo(repo_name,commit_branch,commit_email):
    sFlaskPath = (os.path.dirname(os.path.realpath(__file__)))
    sProgramPath = os.path.abspath(os.path.join(sFlaskPath, os.pardir))
    sProgramPath = sProgramPath + "\\programs\\" + "repo-build.exe"
    sSendEmail = commit_email.replace(">","<").split("<")
    sSendEmail.extend(["seanb@a-p-g.com","seanb@a-p-g.com"])
    sSendEmail = sSendEmail[1]

    if sSendEmail == "": 
        sSendEmail = "sean@theguru.co.uk"

    sInstall = "\\\\SYNOLOGY\\APG-Dropbox\\Shared with GMCO\\release"
    sRepoName = "git@bitbucket.org:"+repo_name 
    sBranch = commit_branch
    
    print sProgramPath
    print sInstall 
    
    pid = subprocess.Popen([sProgramPath,sRepoName,sBranch,sInstall,sSendEmail], creationflags=DETACHED_PROCESS).pid    
    print pid 

@app.route('/release/<filename>')
def release(filename):
    return send_from_directory("d:/flask-files/",filename)

@app.route("/webhook", methods=["GET", "POST"])
def tracking():
    """Endpoint for receiving webhook from bitbucket."""
    if request.method == "POST":
        data = request.get_json()
        commit_author = data["actor"]["username"] 
        repo_name = data["repository"]["full_name"] 

        for event in data["push"]["changes"]:
            new_branch = event["new"]
            if new_branch is not "null":
        
                commit_branch = event["new"]["name"]
                commit_email = event["new"]["target"]["author"]["raw"]

                print ""
                print "Webhook received."
                print "==========================="
                print "Author: %s" % commit_author
                print "Branch: %s" % commit_branch
                print "Repo_Name: %s" % repo_name
                print "email: %s" % commit_email

                if branch_is_valid(commit_branch):
                    build_repo(repo_name,commit_branch,commit_email)
        
                print "----"
                print ""
                return "OK"
    else:
        return display_html(request)

if __name__ == "__main__":
    display_intro()
    app.run(host="0.0.0.0", port=5000, debug=True)  
