[Workspace]
Home=..\
AppSrcPath=.\AppSrc
AppHTMLPath=.\AppHtml
BitmapPath=.\Bitmaps
IdeSrcPath=.\IdeSrc
DataPath=.\Data
DDSrcPath=.\DdSrc
HelpPath=.\Help
ProgramPath=.\Programs
FileList=.\Data\Filelist.cfg
Description=APG Developer Tools

[repo-build]
email=<repo-build always sends to this address>

[SMTP Server]
Server=smtp.sendgrid.net
Username=<smtp username>
Password="<your password>"
Port=25
Sender=<your email>
Sender Name=<your name>
