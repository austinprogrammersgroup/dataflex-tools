
@echo creating secret files
@echo off 

Echo Launch dir: "%~dp0"

if not exist %~dp0\programs\config.ws (
    copy %~dp0\setup\config.ws %~dp0\programs\
) 

if not exist %~dp0\appsrc\secrets.pkg (
    copy %~dp0\setup\secrets.pkg %~dp0\appsrc\
)