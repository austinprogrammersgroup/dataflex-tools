Repo-Build
==========

Tool to facilitate the automatic building of dataflex projects. Projects are automatically checked out of  
Git, and can be compiled, and then released to either zip file, or to a release folder. 

Initial Setup 
----------------------

After cloning the workspace, you'll need to create some files and register some COM objects. 

Firstly, run "initial_setup.bat" to copy the config and secrets.pkg files to their correct locations. 

If you havn't done it already, COM register (regsvr32) the ChilkatAx-9.5.0-win32.dll file 
in COM Components, and add your email component key to secrets.pkg 

Finally, update the settings in config.ws to include your SMTP server settings. 


Basic Tool usage
----------------

```
repo-build {repository} {branch} {output-folder} {email}
```

 - repository. The URL of the repository that you would normally use to clone it. I don't 
   know if http cloning works, but repositories in the format 
   ##git@bitbucket.org:austinprogrammersgroup/dataflex-tools.git##
   do work.
 - branch. The name of the branch to check out. 
   e.g. master, qa, staging, development. 
 - output-folder. If you want to copy the output folder (without zipping or publishing it) 
   to a named, folder, then this is the folder to use. If you are going to be zipping 
   and publishing the app, then make this a temporary folder. 
 - email - the email address of the person who will be told about the build. Other email 
   addresses can be included in the repo-build repository. 
   

Setting up repo-build
---------------------

1. Add the following sections to your repo-build workspace file. (Usually config.ws in your programs folder)

```
[repo-build]
email={your email}

[SMTP Server]
Server={server}
Username={username}
Password={password}
Port={smtp port}
Sender={email of person sending build emails}
Sender Name={name of the person sending emails}
```
   
2. compile repo-build

3. Use RegisterCompiler, if you want to add repo-build (and flexcomp) to your path. 

4. You will need to register the Chilkat COM Component which is in the "COM Component" library. 

   
Converting a repository so it can be built with the repo-build tools. 
----------------

The heart of the repository build is the buildfile.json file in the root folder. This
contains instructions for converting your repository. The file format is JSON. 

e.g. 


```

{
    "repository": "dataflex-tools",
    "emails": ["seanb@a-p-g.com"],
    "output-folder": "d:/flask-files",
    "output-web": "http://seanbamforth.ngrok.io/release/",
    

    "pre-build": ["weird-popup"],
    "compile-programs": [
        "indent","flexcomp","generateManifests",
        "repo-build", "RegisterCompiler"
    ],
    "post-build": [],
    "options": ["manifest","zip"],
    "libraries": [
        ["reports", "git@bitbucket.org:austinprogrammersgroup/dataflex-reports-5.0.1.git", "master"],
        ["query",   "git@bitbucket.org:austinprogrammersgroup/library-vdfquery.git",       "master"],
        ["printing","git@bitbucket.org:austinprogrammersgroup/library-printing.git",       "master"]
    ],

    "release": {
        "programs": ["indent.exe","flexcomp.exe","generateManifests.exe","repo-build.exe","RegisterCompiler.exe","config.ws"],
        "data": ["filelist.cfg"]
    }
    
    
}

```

 - repository. Used to name the release folder, and the zip file that's sent. 
 - emails. JSON array of emails 
 - output-folder. Name of the folder the zip can be downloaded from 
 - output-web. Base web address of the folder the zip can be downloaded from. 
 - pre-build. List of dataflex source files that are compiled and run before / after compilation.
 - compile-programs. list of the programs to be compiled. 
 - options - A JSON array containing any of...
   - zip - zip up the folder, before releasing it. 
   - manifest - generate manifest files.
 - libraries. A JSON array of the libraries used in the project. All libraries must be available 
   to be cloned via Git. Each Library is an array containing...
   - Name
   - Git repository location
   - branch to check out. 
 - release - This JSON Structure contains details of the folders and files to be released.  
 
##Notes.##

  - output-folder and output-web only work with the "zip" option, and are used to release to 
    a zip file, which can be downloaded. 
 
 
pre-build and post-build files
----------------------

Weird-popup.src contains an example of a build source file. This is compiled in dataflex 
and then run. Output from this application should be to *{home}\build-output\{program}.messages*
and *{home}\build-output\{program}.errors*. Messages get sent on the email & errors stop the 
build from being released. These files can be used for pre-compile steps (generating version numbers) 
and post compile steps like unit testing. 

 
*in the example above, home is the workspace home folder, and program is the name of the 
program that has been created. e.g. "run-tests" would be the program name for "run-tests.src" *