Tools
=====

 - Compiler. Use flexcomp from the command line to compile. 
 - ReportList. Finds used and unused crystal reports. 
 - generateManifests. Creates basic Manifest files for all executables. 
 - Indent. Code indenter. 


Initial Setup 
----------------------

After cloning the workspace, you'll need to create some files and register some COM objects. 

Firstly, run "initial_setup.bat" to copy the config and secrets.pkg files to their correct locations. 

If you havn't done it already, COM register (regsvr32) the ChilkatAx-9.5.0-win32.dll file 
in COM Components, and add your email component key to secrets.pkg 

Finally, update the settings in config.ws to include your SMTP server settings. 

*A quick Note: The compiler relies on the compiler OCX, and this changes from version to version of dataflex. 
Normally, this doesn't matter as long as you have the correct version of dataflex on your computer. If you don't, then 
you will need to regernerate VdfCompCtrl.pkg. This is currently set at Dataflex 18.2*


Indent - CodeIndenter
------------

After compiling, Add the following to your tools menu to auto-indent from inside the Studio.


```
  command: {path}\dataflex-compile\Programs\indent.exe 
  parameters: <save><file> -notify 
```



Compiler
--------

I've written a wrapper for the Dataflex Compiler that allows it to be run from the command line. 

To set the program up, open "RegisterCompiler.src" in the IDE, compile and run it. Click the "Register and Compile" button to set up the console mode environment. 

To compile a program, open a command prompt, move to a folder in your workspace and type 
"flexcomp programToCompile.src"

To compile a program in "console" mode, use the --console switch when compiling, or add a {Console=True} directive in your source code file. 

The program has been tested for all versions of Dataflex from 16.1. Change the workspace string in Compiler.sws to compile it against other versions. 

The console mode compiler has the following features: 

- run : Run the program after compile
- version {version} : Compile against different dataflex versions. 
- console : force console mode for the application
- win : force windows mode for the application
- register : register application so can be run from anywhere
- force-register : force register application so can be run from anywhere

Here I am with a giant beard, muttering my way through 15 minutes of demo of the setup and basic use of the flexcomp tool: 
https://www.youtube.com/watch?v=JcNHc9-881E

I see three uses for this tool. These are. 

1. Creation of console mode tools that can be easily run against workspaces. e.g. listing tables in filelist.cfg; creating source code files based on templates; database cleanup and export tools. 

2. Creation of batch files. The more we use the web, the more there's a need for single use batch tools. Showln is useful, but it closes when the program closes. It's useful if these tools can be run from the console mode and provide feedback while they are running. 

3. Build Scripts. DFCOMP.exe can be run from a build script, but this should make things easier. 

## Problems. 

I get an appcrash sometimes when compiling. I'm not entirely sure why this is. It would be great to talk to someone who understands COM and the message Loop as to why this would be. 

It should go without saying, but this is not the console mode compiler. It's the Windows Compiler run from the console.