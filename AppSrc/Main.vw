Use Windows.pkg
Use DFClient.pkg
Use cTextEdit.pkg


Class cCodeIndenter is a cObject
	
	Procedure construct_object
		Forward Send Construct_Object
		Property Integer piLevel
		Property String[] psIndentWords
		Property String[] psUnIndentWords
		Property String[] psIgnoreWords
		Property String[]  ptArray
		Property Boolean pbCaseBlock
	End_Procedure
	
	Procedure init
		String[] vEmpty
		Set ptArray to vEmpty
		Set piLevel to 0
	End_Procedure
	
	Function TabCount Returns String
		String sData
		Integer iCtr iCount
		Get piLevel to iCount
		For iCtr from 1 to iCount
			Move (sData + "    "   ) to sData
		Loop
		Function_Return sData
	End_Function
	
	Procedure adjustTabCount Integer iDir
		Integer iLevel
		Get piLevel to iLevel
		Move (iLevel+ iDir) to iLevel
		Set piLevel to iLevel
	End_Procedure
	
	Procedure AddIgnoreWord String sWord
		String[] sData
		Integer iSize
		Get psIgnoreWords to sData
		Move (SizeOfArray(sData)) to iSize
		Move sWord to sData[iSize]
		Set psIgnoreWords to sData
	End_Procedure
	
	
	
	Procedure AddIndentWord String sWord
		String[] sData
		Integer iSize
		Get psIndentWords to sData
		Move (SizeOfArray(sData)) to iSize
		Move sWord to sData[iSize]
		Set psIndentWords to sData
	End_Procedure
	
	Procedure AddUnIndentWord String sWord
		String[] sData
		Integer iSize
		Get psUnIndentWords to sData
		Move (SizeOfArray(sData)) to iSize
		Move sWord to sData[iSize]
		Set psUnIndentWords to sData
	End_Procedure
	
	Function hasIfBegin String sLine Returns Boolean
		UChar[] vArray
		Boolean bHas
		Integer iSize
		String sStart sFin
		Move (StringToUCharArray( (Uppercase(sLine)))) to vArray
		Move (SizeOfArray(vArray)) to iSize
		Move (UCharArrayToString( vArray,2)) to sStart
		Move (UCharArrayToString( vArray,iSize, (iSize-5))) to sFin
		Move ((sStart='IF') and (sFin='BEGIN')) to BHas
		Function_Return bHas
	End_Function
	
	Function hasCase String sLine Returns Boolean
		UChar[] vArray
		Boolean bHas
		Integer iPos1 iPos2
		String sStart sFin
		Move (Uppercase(sLine)) to sLine
	Move (Pos('CASE (',sLine)) to iPos1
	Move (Pos('CASE ELSE',sLine)) to iPos2
		
		Function_Return (iPos1 or iPos2)
	End_Function
	
	Function HasIgnoreWord String sLine Returns Boolean
		String[] sData
		Integer iSize iCtr
		Boolean bHas
		String sOne
		Get psIgnoreWords to sData
		Move (uppercase(sLine)) to sLine
		Move (SizeOfArray(sData)) to iSize
		Decrement iSize
		For iCtr from 0 to iSize
			Move (uppercase(sData[iCtr])) to sOne
			If ((Pos(sOne,sLine))=1) Begin
				Move True to bHas
			End
		Loop
		Function_Return bHas
	End_Function
	
	Function HasIndentWord String sLine Returns Boolean
		String[] sData
		Integer iSize iCtr
		Boolean bHas
		String sOne
		Get psIndentWords to sData
		Move (uppercase(sLine)) to sLine
		Move (SizeOfArray(sData)) to iSize
		Decrement iSize
		For iCtr from 0 to iSize
			Move (uppercase(sData[iCtr])) to sOne
			If ((Pos(sOne,sLine))=1) Begin
				Move True to bHas
			End
		Loop
		Function_Return bHas
		
	End_Function
	
	Function hasUnIndentWord String sLine Returns Boolean
		String[] sData
		Integer iSize iCtr
		Boolean bHas
		String sOne
		Get psUnIndentWords to sData
		Move (uppercase(sLine)) to sLine
		Move (SizeOfArray(sData)) to iSize
		Decrement iSize
		For iCtr from 0 to iSize
			Move (uppercase(sData[iCtr])) to sOne
			If ((Pos(sOne,sLine))=1) Begin
				Move True to bHas
			End
		Loop
		Function_Return bHas
	End_Function
	
	Function adjustLine String sOrigLine Returns String
		Integer iLevel
		Boolean bComments
		String sTab sLine
		Move (Trim(sOrigLine)) to sLine
		Move (Pos(("/"+"/"),sLine)) to bComments
		Get TabCount to sTab
		Move (sTab+sLine) to sLine
		Function_Return sLine
	End_Function
	
	Procedure ProcessLine String sOrigLine
		String sData sTest sLine
		UChar[] ptLine
		Boolean bIndent bUnIndent bIgnore bIfBegin bCase
		Move (trim(sOrigLine))  to sLine
		If (Pos("SET PSHTML",(UPPERCASE(SlINE)))) BEGIN
			Move 1 to WINDOWINDEX
		End
		Get HasCase         sLine to bCase
		Get HasIfBegin      sLine to bIfBegin
		Get HasIgnoreWord   sLine to bIgnore
		Get HasIndentWord   sLine to bIndent
		Get HasUnIndentWord sLine to bUnIndent
		
		Case Begin
		Case (bIgnore)
			Get adjustLine sOrigLine to sLine
			Case Break
			
		Case (bCase)
			Send adjustTabCount -1
			Get adjustLine sOrigLine to sLine
			Send adjustTabCount 1
			
			Case Break
			
		Case (bIndent or bIfBegin)
			Get adjustLine sOrigLine to sLine
			Send adjustTabCount 1
			Case Break
		Case (bUnIndent)
			Send adjustTabCount -1
			Get adjustLine sOrigLine to sLine
			Case Break
			
		Case Else
			Get adjustLine sOrigLine to sLine
			Case Break
		Case End
		
		Send addLine sLine
	End_Function
	
	Procedure addLine String sLine
		String[] vLines
		Integer iSize
		Get ptArray to vLines
		Move (SizeOfArray(vLines)) to iSize
		Move (sLine + (Character(13)) + (Character(10)))  to vLines[iSize]
		Set ptArray to vLines
	End_Procedure
	
	Procedure End_Construct_Object
		
// extend as required
// would be better loaded form an INI file
//		
		Forward Send End_Construct_Object
		
		Send addIgnoreWord 'Function_return'
		Send addIgnoreWord 'Procedure_return'
		Send AddIgnoreWord 'Case Break'
		
		Send AddIndentWord 'Object'
		Send AddIndentWord 'For '
		
		Send AddIndentWord 'Begin'
		Send AddIndentWord 'Function'
		Send AddIndentWord 'For_all'
		Send AddIndentWord 'Procedure'
		Send AddIndentWord 'Class'
		Send AddIndentWord 'Case Begin'
		Send AddIndentWord 'Struct'
		Send AddIndentWord 'Enum_List'
		Send AddIndentWord 'For_Each'
		Send AddIndentWord 'While'
		
		Send AddIndentWord 'Else Begin'
		
		Send AddUnIndentWord 'End_Object'
		Send AddUnIndentWord 'End'
		Send AddUnIndentWord 'End_for_all'
		Send AddUnIndentWord 'End_for_Each'
		Send AddUnIndentWord 'Case End'
		
		Send AddUnIndentWord 'End_Function'
		Send AddUnIndentWord 'End_Procedure'
		Send AddUnIndentWord 'End_Class'
		Send AddUnIndentWord 'Case Break'
		
		Send AddUnIndentWord 'Cd_End_Object'
		Send AddUnIndentWord 'End_Struct'
		Send AddUnIndentWord 'End_Enum_list'
		Send AddUnIndentWord 'Loop'
		
	End_Procedure
	
End_Class




Activate_View Activate_oMain for oMain
Object oMain is a dbView
	
	Set Border_Style to Border_Thick
	Set Size to 291 539
	Set Location to 2 2
	Set Label to "Main"
	Set pbAutoActivate to True
	Set Caption_Bar to False
	
	Object oCodeIntenter is a cCodeIndenter
	End_Object
	
	Object oOpenDialog1 is an OpenDialog
		Handle hWorkspace
		String sFolder
		Get phoWorkspace of ghoApplication to hWorkspace
		Get psAppSrcPath of hWorkspace to sFolder
		Set Filter_String to  'All Source|*.src;*.pkg;*.wo;*.vw;*.DD;*.bp'
		Set Initial_Folder to sFolder
		Set Filter_Index to 1
	End_Object
	
	Object oFileName is a Form
		Set Size to 13 215
		Set Location to 12 50
		Set Prompt_Button_Mode to PB_PromptOn
		Set Label to "Select File:"
		Set Label_Col_Offset to 0
		Set Label_Justification_Mode to JMode_Right
		
		Procedure prompt
			Boolean bOpen bReadOnly
			String sFileTitle sFileName
			String[] sSelectedFiles
			Get Show_Dialog of oOpenDialog1 to bOpen
			If bOpen Begin
				Get TickReadOnly_State of oOpenDialog1 to bReadOnly
				Get File_Title of oOpenDialog1 to sFileTitle
				Get Selected_Files of oOpenDialog1 to sSelectedFiles
				Set Value to sSelectedFiles[0]
			End
		End_Procedure
		
		
		
		
	End_Object
	
	Object oButton1 is a Button
		Set Location to 270 482
		Set Label to 'Save'
		Set peAnchors to anBottomRight
		
		Procedure OnClick
			Send saveFile of oEdit
		End_Procedure
		
	End_Object
	
	Object oEdit is a cTextEdit
		Set Size to 225 523
		Set Location to 41 9
		Set peAnchors to anAll
		Set pbWrap to False
		
		Procedure LoadFile
			String sFile
			Get Value of oFileName to sFile
			Set Value to ''
			Send Read sFile
		End_Procedure
		
		Procedure SaveFile
			String sFile
			Get Value of oFileName to sFile
			Send Write sFile
		End_Procedure
		
		Procedure ProcessFile
			Integer iLines iCtr iSize
			String sLine sData
			String[] varray
			Get Line_Count to iLines
			Decrement iLines
			For iCtr from 0 to iLines
				Get Line iCtr to sLine
				Send ProcessLine of oCodeIntenter (trim(sLine))
				
			Loop
			
		End_Procedure
		
		Procedure display
			String[] vArray
			Integer iSize iCtr
			String sLine
			Get ptArray of oCodeIntenter to vArray
			Set Dynamic_Update_State to False
			Set Value to ''
			Move (SizeOfArray(varray)) to iSize
			Decrement iSize
			For iCtr from 0 to iSize
				Send AppendText vArray[ictr]
			Loop
			Set Dynamic_Update_State to True
		End_Procedure
		
	End_Object
	
	Object oLoadBtn is a Button
		Set Location to 11 273
		Set Label to 'Load'
		
		Procedure OnClick
			Send loadFile of oEdit
		End_Procedure
		
	End_Object
	
	Object oButton1 is a Button
		Set Location to 271 414
		Set Label to 'Process'
		Set peAnchors to anBottomRight
		
		Procedure OnClick
			Send init of oCodeIntenter
			Send ReadFromDisk
			Send ProcessFile of oEdit
			Send Display     of oEdit // disp
		End_Procedure
		
	End_Object
	
	Procedure ReadFromDisk 
		String sFile
		Integer iSize iFileSize
		UChar[] vFile
		Get Value of oFilename to sFile
		// read a sequential file
		Direct_Input channel 5 sFile
		If (SeqEof) Begin
			Procedure_Return
		End
		// read entire file into UChar array
		Read_Block channel 5 vFile -1
		Close_Input channel 5
		Move (SizeOfArray(vFile)) to iFileSize
		Get_Argument_Size to iSize
		If (iFileSize> iSize) Begin
			Set_Argument_Size (iFileSize+128)
		End
	End_Procedure
	
	
	Procedure popup
		String sFile
		Forward Send popup
		Get psFileName of ghoApplication to sFile
		Set Value of oFilename to sFile
		
		If (sFile<>'') Begin
			Send KeyAction of  oLoadBtn
		End
	End_Procedure
End_Object


